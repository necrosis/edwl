#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <xkbcommon/xkbcommon.h>


unsigned long
djb2hash(const char* str)
{
  unsigned long hash = 5381;
  int c;

  if (str == NULL || strlen(str) == 0)
    return 0;

  while (c = *str++)
    hash = ((hash << 5) + hash) + c;

  return hash;
}

bool exists(unsigned long array[], unsigned long value, size_t max)
{
  for (int i = 0; i < max; i++)
  {
    if (array[i] == value)
      return true;
  }
  return false;
}

int
main(void)
{
	struct xkb_context *context;
	/* Allow generate keys with a different layout and variant.
	 * You can also use XKB_DEFAULT_* environmental variables and let this as is */
	struct xkb_rule_names rules = {
		0
	};
	struct xkb_keymap *keymap;
	struct xkb_state *state;
	xkb_keycode_t keycode, min_keycode, max_keycode;
	int i, nsyms;
	const xkb_keysym_t *syms;
	char keyname[64];
  unsigned long codes[512], hash;
  size_t codes_written = 0;
	FILE *f = fopen("keys.h", "w");
	if (!f) {
		perror("Couldn't open keys.h");
		return EXIT_FAILURE;
	}

	if (!(context = xkb_context_new(XKB_CONTEXT_NO_FLAGS))) {
		fputs("Couldn't create xkbcommon context\n", stderr);
		return EXIT_FAILURE;
	}

	if (!(keymap = xkb_keymap_new_from_names(context, &rules,
			XKB_KEYMAP_COMPILE_NO_FLAGS))) {
		fputs("Couldn't create xkbcommon keymap\n", stderr);
		return EXIT_FAILURE;
	}

	if (!(state = xkb_state_new(keymap))) {
		fputs("Couldn't create xkbcommon state\n", stderr);
		return EXIT_FAILURE;
	}

	min_keycode = xkb_keymap_min_keycode(keymap);
	max_keycode = xkb_keymap_max_keycode(keymap);

	for (keycode = min_keycode; keycode <= max_keycode; keycode++) {
		nsyms = xkb_state_key_get_syms(state, keycode, &syms);
		for (i = 0; i < nsyms; i++) {
			xkb_keysym_get_name(syms[i], keyname, sizeof(keyname) / sizeof(keyname[0]));

      // to prevent keycode duplication (happens on some keyboards)
      hash = djb2hash(keyname);
      if (!exists(codes, hash, codes_written))
      {
        codes[codes_written] = hash;
        ++codes_written;

        fprintf(f, "#define Key_%-24s %d\n", keyname, keycode);
      }
		}
	}

	xkb_state_unref(state);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);
}
