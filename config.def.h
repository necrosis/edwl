#ifndef COLOR
#define COLOR_(X) {((X & 0xFF000000) >> 24) / 255.0, ((X & 0x00FF0000) >> 16) / 255.0, ((X & 0x0000FF00) >> 8) / 255.0, (X & 0x000000FF) / 255.0}
#define COLOR(X) COLOR_( 0x ## X)
#endif

/* appearance */
static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 1;  /* border pixel of windows */
static const int lockfullscreen     = 1;  /* 1 will force focus on the fullscreen window */
static const int bypass_surface_visibility = 0; /* 1 means idle inhibitors will disable idle tracking even if it's surface isn't visible  */
static const float rootcolor[]      = COLOR( 222222ff );
static const float bordercolor[]    = COLOR( 7f7f7fff );
static const float focuscolor[]     = COLOR( ff0000ff );
static const float urgentcolor[]    = COLOR( ff0000ff );
static const float fullscreen_bg[]  = COLOR( 1a1a1aff );

/* bar */
static const float barcolor[]       = COLOR( 005478ff );
static const float barbackcolor[]   = COLOR( 000000ff );
static const float baractivecolor[] = COLOR( ffffffff );
static const unsigned int barheight = 20;
static const char barfontname[]     = "Times new roman,Sans";
static const float barfontcolor[]   = COLOR( ffffffff );
static const unsigned int barfontsize = 12;
static const int baralwaysontop     = 0;
static const int unsetfullscreenonmap = 1;

/* background image */
static const char backgroundimage[] = "/usr/local/share/edwl/background.png";

/*  lock  */
static const float lockcolor[]      = COLOR( 00ff00ff );
static const char codetounlock[]    = "unlock";

/*  gaps  */
static const unsigned int gapsize   = 3;

/*  devour */
static const char devourcommand[]   = "edwldevour";

/* Tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
  /* app_id     title       tags mask     isfloating   monitor */
  /* examples:
  { "Gimp",     NULL,       0,            1,           -1 },
  { "firefox",  NULL,       1 << 8,       0,           -1 },
  */
};

/* layout(s) */
static const Layout layouts[] = {
  /* symbol     arrange function */
  { "[]=",      tile },
  { "><>",      NULL },    /* no layout function means floating behavior */
  { "[M]",      monocle },
};

/* monitors
 * The order in which monitors are defined determines their position.
 * Non-configured monitors are always added to the left. */
static const MonitorRule monrules[] = {
  /* name       mfact nmaster scale  layout          rotate/reflect               x   y  round  adaptive sync   */
  /* example of a HiDPI laptop monitor:
  */
  /* defaults */
  { NULL,       0.55, 3,     1,     &layouts[0],    WL_OUTPUT_TRANSFORM_NORMAL, -1, -1, 0,     1 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
  /* can specify fields: rules, model, layout, variant, options */
  /* examples:  */
  /* .options = "grp:alt_shift_toggle", */
  /* .layout = "us,ru" */
};

/* Manual layout switching (for some weird keyboards  */
static const char *kblayouts[] = {"us"};

static const int repeat_rate = 25;
static const int repeat_delay = 600;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;

/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;

/* You can choose between:
LIBINPUT_CONFIG_CLICK_METHOD_NONE       
LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS       
LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER 
*/
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;

/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;

/* You can choose between:
LIBINPUT_CONFIG_TAP_MAP_LRM -- 1/2/3 finger tap maps to left/right/middle
LIBINPUT_CONFIG_TAP_MAP_LMR -- 1/2/3 finger tap maps to left/middle/right
*/
static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LRM;

/* If you want to use the windows key change this to WLR_MODIFIER_LOGO */
#define MODKEY WLR_MODIFIER_LOGO
#define TAGKEYS(KEY,TAG) \
  { MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
  { MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
  { MODKEY|WLR_MODIFIER_SHIFT, KEY,            tag,             {.ui = 1 << TAG} }, \
  { MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,KEY,toggletag,  {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[] = { "foot", NULL };
static const char *menucmd[] = { "bemenu-run", NULL };

#include "keys.h"
static const Key keys[] = {
  /* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
  /* modifier                  key                 function        argument */
  { MODKEY,                    Key_r,          spawn,          {.v = menucmd} },
  { MODKEY,                    Key_Return,     spawn,          {.v = termcmd} },
  { MODKEY|WLR_MODIFIER_CTRL,  Key_b,          screenlock,     {0} },
  { MODKEY,                    Key_b,          toggleconstraint,{0} },
  { MODKEY,                    Key_j,          focusstack,     {.i = +1} },
  { MODKEY,                    Key_k,          focusstack,     {.i = -1} },
  { MODKEY,                    Key_i,          incnmaster,     {.i = +1} },
  { MODKEY,                    Key_d,          incnmaster,     {.i = -1} },
  { MODKEY,                    Key_h,          setmfact,       {.f = -0.05} },
  { MODKEY,                    Key_l,          setmfact,       {.f = +0.05} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_j,          scrollclients,  {.i = 1} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_k,          scrollclients,  {.i = -1} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_Return,     zoom,           {0} },
  { MODKEY,                    Key_Tab,        view,           {0} },
  { MODKEY,                    Key_q,          killclient,     {0} },
  { MODKEY,                    Key_t,          setlayout,      {.v = &layouts[0]} },
  { MODKEY,                    Key_g,          setlayout,      {.v = &layouts[1]} },
  { MODKEY,                    Key_m,          setlayout,      {.v = &layouts[2]} },
  { MODKEY,                    Key_space,      nextlayout,     {0} },
  { MODKEY,                    Key_s,          togglesticky,   {0} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_f,          togglefloating, {0} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_c,          togglecursor,   {0} },
  { MODKEY,                    Key_f,        togglefullscreen, {0} },
  { MODKEY,                    Key_o,          view,           {.ui = ~0} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_parenright, tag,            {.ui = ~0} },
  { MODKEY,                    Key_Tab,        focusmon,       {.i = WLR_DIRECTION_LEFT} },
  { MODKEY,                    Key_period,     focusmon,       {.i = WLR_DIRECTION_RIGHT} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_less,       tagmon,         {.i = WLR_DIRECTION_LEFT} },
  { MODKEY|WLR_MODIFIER_SHIFT, Key_period,     tagmon,         {.i = WLR_DIRECTION_RIGHT} },
  TAGKEYS(                     Key_1,                   0),
  TAGKEYS(                     Key_2,                   1),
  TAGKEYS(                     Key_3,                   2),
  TAGKEYS(                     Key_4,                   3),
  TAGKEYS(                     Key_5,                   4),
  TAGKEYS(                     Key_6,                   5),
  TAGKEYS(                     Key_7,                   6),
  TAGKEYS(                     Key_8,                   7),
  TAGKEYS(                     Key_9,                   8),
  { MODKEY|WLR_MODIFIER_SHIFT, Key_q,          quit,           {0} },

  /* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
  { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,Key_BackSpace, quit, {0} },
#define CHVT(KEY,n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT, KEY, chvt, {.ui = (n)} }
  CHVT(Key_F1, 1), CHVT(Key_F2,  2),  CHVT(Key_F3,  3),  CHVT(Key_F4,  4),
	CHVT(Key_F5, 5), CHVT(Key_F6,  6),  CHVT(Key_F7,  7),  CHVT(Key_F8,  8),
	CHVT(Key_F9, 9), CHVT(Key_F10, 10), CHVT(Key_F11, 11), CHVT(Key_F12, 12),
};

static const Button buttons[] = {
  { MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
  { MODKEY, BTN_MIDDLE, togglefloating, {0} },
  { MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};

static const char *autorun[] = {
/* "sh", "-c", "${XDG_CONFIG_HOME}/edwl/utility.sh", NULL */
  NULL, NULL
};
